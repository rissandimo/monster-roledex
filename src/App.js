import React, { Component} from 'react';
import './App.css';

// Componenets
import { CardList } from './components/card-list/card-list.component'
import { SearchBox } from './components/search-box/search-box.component';

class App extends Component {

  constructor(){
    super();

    this.state = {
      monsters: [],
      searchField: ''
    };

  }

  componentDidMount(){
    fetch('http://jsonplaceholder.typicode.com/users')
    .then(response => response.json()) // convert to json b/c javascript can only understand json
    .then(users => this.setState({monsters: users})); // monster name
  }

  handleChange = (e) => {
    this.setState({ searchField: e.target.value})
  }

    render(){

      const { monsters, searchField } = this.state;
      const filteredMonsters = monsters.filter(monster => 
        monster.name.toLowerCase().includes(searchField.toLowerCase()));

      return (
        <div className="App">
         <SearchBox
           placeholder='Search Monsters'
           handleChange={this.handleChange} />
         <CardList monsters={filteredMonsters} />
        </div>
      );
    }
}

export default App;
